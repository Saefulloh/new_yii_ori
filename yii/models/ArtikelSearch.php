<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Artikel;

/**
 * ArtikelSearch represents the model behind the search form of `app\models\Artikel`.
 */
class ArtikelSearch extends Artikel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_artikel', 'kategori', 'status'], 'integer'],
            [['judul_indo', 'judul_en', 'isi_indo', 'isi_en', 'author', 'tgl_buat', 'update', 'update_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Artikel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_artikel' => $this->id_artikel,
            'kategori' => $this->kategori,
            'status' => $this->status,
            'tgl_buat' => $this->tgl_buat,
            'update' => $this->update,
        ]);

        $query->andFilterWhere(['like', 'judul_indo', $this->judul_indo])
            ->andFilterWhere(['like', 'judul_en', $this->judul_en])
            ->andFilterWhere(['like', 'isi_indo', $this->isi_indo])
            ->andFilterWhere(['like', 'isi_en', $this->isi_en])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
