<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel".
 *
 * @property int $id_artikel
 * @property string $judul_indo
 * @property string $judul_en
 * @property string $isi_indo
 * @property string $isi_en
 * @property string $author
 * @property int $kategori
 * @property int $status
 * @property string $tgl_buat
 * @property string $update
 * @property string $update_by
 */
class Artikel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artikel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isi_indo', 'isi_en'], 'string'],
            [['kategori', 'status'], 'integer'],
            [['tgl_buat', 'update'], 'safe'],
            [['judul_indo', 'judul_en', 'author', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_artikel' => 'Id Artikel',
            'judul_indo' => 'Judul Indo',
            'judul_en' => 'Judul En',
            'isi_indo' => 'Isi Indo',
            'isi_en' => 'Isi En',
            'author' => 'Author',
            'kategori' => 'Kategori',
            'status' => 'Status',
            'tgl_buat' => 'Tgl Buat',
            'update' => 'Update',
            'update_by' => 'Update By',
        ];
    }
}
